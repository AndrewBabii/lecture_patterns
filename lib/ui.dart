import 'package:flutter/material.dart';
import 'package:lecture_patterns/model.dart';

class Ui extends StatelessWidget {
  Ui(this.model);
  Model model;
  @override
  Widget build(BuildContext context) {


    return Row(
      children: [
        Text(model.name),
        Text(model.id),
        Text(model.email),
        Text(model.phone),
      ],
    );
  }
}
