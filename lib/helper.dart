class Helper{

  Helper._privateConstructor();

  static final Helper _instance = Helper._privateConstructor();

  static Helper get instance => _instance;
}